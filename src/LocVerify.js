import React, { Component } from "react";
import { StyleSheet, Text, View, Button, ToastAndroid, AsyncStorage } from "react-native";


export default class LocVerify extends Component {
  constructor(props) {
    super(props);

    this.state = { resultloc:" ", scanner: undefined, status:null, jadwalid:null };
  }
  componentDidMount() {
    //The code bellow will receive the props passed by QRCodeScannerScreen
    const resultloc = this.props.navigation.getParam("data", "No data read");
    const scanner = this.props.navigation.getParam("scanner", () => false);
    AsyncStorage.getItem('jadwalid', (error, result) => {
        if (result) {
            this.setState({jadwalid: result});
            this.verifloc();
        }
    });
    this.setState({ resultloc: resultloc, scanner: scanner });
  }
  verifloc(){
    fetch("http://192.168.42.224:3000/sisqr/valloc", {
        method: 'POST',
        body: JSON.stringify({
          jadwalid:this.state.jadwalid,
          lokasi:this.state.resultloc
        }),
        headers: {
          "Content-type": "application/json"
        }
      }).then(res => res.json())
        .then(
          (result) => {
            if(result.message=="failed"){
              ToastAndroid.show('failed', ToastAndroid.SHORT);
            }else{
              console.log(result),
              ToastAndroid.show('location verified, congrats!', ToastAndroid.SHORT);
              this.setState({
                  status:"verified"
              });
            }
          },
          (error) => {
            this.setState({
              error
            });
          }
        )
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>status : {this.state.status}</Text>
        <Button
          title={"Home"}
          onPress={() => this.props.navigation.popToTop()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  text: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  listtext: {
    fontSize: 20,
    textAlign: "left",
    margin: 10
  }
});

