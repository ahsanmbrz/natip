import { createAppContainer, createStackNavigator } from "react-navigation";

import Home from "./Home";
import QRCodeScannerScreen from "./QRCodeScannerScreen";
import QRCodeData from "./QRCodeData"; // <--- New Line
import UserQRScannerScreen from "./UserQRScannerScreen";
import UserList from "./UserList";
import LocQRScannerScreen from "./LocQRScannerScreen";
import LocVerify from "./LocVerify";

const mainStack = createStackNavigator(
  {
    Home: Home,
    QRCodeScannerScreen: QRCodeScannerScreen,
    QRCodeData: QRCodeData,
    UserQRScannerScreen: UserQRScannerScreen,
    UserList: UserList,
    LocQRScannerScreen: LocQRScannerScreen,
    LocVerify: LocVerify // <--- New line
  },
  { defaultNavigationOptions: { header: null } }
);

const AppContainer = createAppContainer(mainStack);

export default AppContainer;
