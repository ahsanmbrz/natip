import React, { Component } from "react";
import { StyleSheet, Text, View, Button, ToastAndroid, AsyncStorage } from "react-native";


export default class UserList extends Component {
  constructor(props) {
    super(props);

    this.state = { resultdata:" ",showbutton:false, scanner: undefined, id:null,username:null, list:[],anggota:null, jadwalid:null, status:null };
  }
  componentDidMount() {
    //The code bellow will receive the props passed by QRCodeScannerScreen
    const resultdata = this.props.navigation.getParam("data", null);
    const scanner = this.props.navigation.getParam("scanner", () => false);
    AsyncStorage.getItem('jadwalid', (error, result) => {
        if (result) {
            this.setState({jadwalid: result});
            this.updateanggota();
        }
    });
    this.setState({ resultdata: resultdata, scanner: scanner });
  }
  updateanggota(){
    fetch("http://192.168.42.224:3000/sisqr/upang", {
        method: 'POST',
        body: JSON.stringify({
            jadwalid: this.state.jadwalid,
            id:this.state.resultdata
        }),
        headers: {
            "Content-type": "application/json"
        }
}).then(res => res.json())
  .then(
    (result) => {
      if(result.status=="failed"){
        ToastAndroid.show('Validity Failed', ToastAndroid.SHORT);
      }else{
      ToastAndroid.show('User Found!', ToastAndroid.SHORT);
      this.showregistereduser();
    }
    },
    (error) => {
      this.setState({
        error,
      });
    }
  )
  }
  showregistereduser(){
    fetch("http://192.168.42.224:3000/sisqr/showiklan", {
			method: 'POST',
			body: JSON.stringify({
                id:this.state.jadwalid,
			}),
			headers: {
				"Content-type": "application/json"
			}
    }).then(res => res.json())
      .then(
        (result) => {
          console.log(result),
          this.setState({
            list:result.result.hasil.user
          });
          this.listverification();
        },
        (error) => {
          this.setState({
            error
          });
        }
      )
  }
  listverification(){
      let x = 0;
      for(i=0;i<this.state.list.length;i++){
          if(this.state.list[i].ischecked==true){
              x=x+1;
          }
          if(x==this.state.list.length){
              this.setState({
                    status:"allverified",
                    showbutton:true
              });
          }
      }
  }
  scanAgain() {
    this.state.scanner.reactivate();
    this.props.navigation.goBack();
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>status : {this.state.status}</Text>
        <Button
          title={"Home"}
          onPress={() => this.props.navigation.popToTop()}
        />
        <Button
          title={"scan lagi"}
          onPress={() => this.scanAgain()}
        />
        {this.state.showbutton &&
            <Button
            title={"verif lokasi"}
            onPress={() => this.props.navigation.navigate("LocQRScannerScreen")}
            />
        }
        { this.state.list.map((item, key)=>(
         <Text key={key} style={styles.listtext}> username: {item.username} status:{String(item.ischecked)}</Text>)
         )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  text: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  listtext: {
    fontSize: 20,
    textAlign: "left",
    margin: 10
  }
});

