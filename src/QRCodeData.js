import React, { Component } from "react";
import { StyleSheet, Text, View, Button, ToastAndroid, AsyncStorage } from "react-native";


export default class QRCodeData extends Component {
  constructor(props) {
    super(props);

    this.state = { qrCodeData: " ", scanner: undefined, id:null,username:null, list:[],anggota:null };
  }
  componentDidMount() {
    //The code bellow will receive the props passed by QRCodeScannerScreen
    const qrCodeData = this.props.navigation.getParam("data", "No data read");
    const scanner = this.props.navigation.getParam("scanner", () => false);
    this.setState({ qrCodeData: qrCodeData, scanner: scanner });
    fetch("http://192.168.42.224:3000/sisqr/validate", {
			method: 'POST',
			body: JSON.stringify({
        bus: qrCodeData,
        pj:"p2"
			}),
			headers: {
				"Content-type": "application/json"
			}
    }).then(res => res.json())
      .then(
        (result) => {
          if(result.status=="failed"){
            ToastAndroid.show('Validity Failed', ToastAndroid.SHORT);
            this.props.navigation.popToTop();
          }else{
          console.log(result),
          ToastAndroid.show('User Found!', ToastAndroid.SHORT);
          this.setState({
            id: result.result.hasil._id
          });
          this.saveid();
        }
        },
        (error) => {
          this.setState({
            error,
            id: "not found",
            username:"not found"
          });
        }
      )
  }
  saveid(){
      AsyncStorage.setItem('jadwalid', this.state.id);
  }
  addtojadwal(){
      fetch("http://192.168.42.224:3000/sisqr/tojadwal", {
        method: 'POST',
        body: JSON.stringify({
          jadwalid:"5d2597a45512222a74c236af",
          id: this.state.id,
          username : this.state.username
        }),
        headers: {
          "Content-type": "application/json"
        }
      }).then(res => res.json())
        .then(
          (result) => {
            if(result.message=="failed"){
              ToastAndroid.show('Insert Failde, data found in index', ToastAndroid.SHORT);
            }else{
              console.log(result),
              ToastAndroid.show('User Inserted!', ToastAndroid.SHORT);
              this.showregistereduser();
            }
          },
          (error) => {
            this.setState({
              error
            });
          }
        )
  }
  updatejadwal(){
    fetch("http://192.168.42.224:3000/sisqr/upang", {
			method: 'POST',
			body: JSON.stringify({
        jadwalid:"5d2597a45512222a74c236af",
        id: this.state.id
			}),
			headers: {
				"Content-type": "application/json"
			}
    }).then(res => res.json())
      .then(
        (result) => {
          console.log(result),
          ToastAndroid.show('Data Updated', ToastAndroid.SHORT);
          this.showregistereduser();
        },
        (error) => {
          this.setState({
            error
          });
        }
      )
  }
  showregistereduser(){
    fetch("http://192.168.42.224:3000/sisqr/showiklan", {
			method: 'POST',
			body: JSON.stringify({
        id:"5d2597a45512222a74c236af",
			}),
			headers: {
				"Content-type": "application/json"
			}
    }).then(res => res.json())
      .then(
        (result) => {
          console.log(result),
          this.setState({
            list:result.result.hasil.user
          });
        },
        (error) => {
          this.setState({
            error
          });
        }
      )
  }
  scanQRCodeAgain() {
    this.state.scanner.reactivate();
    this.props.navigation.goBack();
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>Kodenya: {this.state.qrCodeData}</Text>
        <Text>ID jadwal : {this.state.id}</Text>
        <Button
          title={"Home"}
          onPress={() => this.props.navigation.popToTop()}
        />
        <Button
          title={"scan anggota"}
          onPress={() => this.props.navigation.navigate("UserQRScannerScreen")}
        />
        { this.state.list.map((item, key)=>(
         <Text key={key} style={styles.listtext}> username: {item.username} status:{String(item.ischecked)}</Text>)
         )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  text: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  listtext: {
    fontSize: 20,
    textAlign: "left",
    margin: 10
  }
});
